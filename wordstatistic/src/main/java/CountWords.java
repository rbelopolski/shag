import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.HashMap;
import java.util.List;

public class CountWords {

    public static final String SRC_MAIN_RESOURCES = "src\\\\main\\resources\\";

    public void findFiles(File rootDirectory, int depth) throws IOException {

        File[] listOfFiles = rootDirectory.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            String iName = listOfFiles[i].getName();
            if (listOfFiles[i].isFile()) {
                if (iName.endsWith(".txt") || iName.endsWith(".TXT")) {
                    for (int j = 0; j < depth; j++) System.out.print("\t");
                    System.out.println("File: "+iName);
                }

                List<String> lines = FileUtils.readLines(new File(SRC_MAIN_RESOURCES + iName));
                //System.out.println(lines);
                for (String line : lines) {
                    String[] words = line.split("[\\.\\s-]+");

                    HashMap<String, Integer> map = new HashMap<String, Integer>();
                    System.out.println("Слова в списке книг и количество их повторений: ");
                    for (String secondWord : words) {
                        Integer count = map.get(secondWord);
                        if (count == null) {
                            map.put(secondWord, 1);//если нет в списке, добавить со значением 1
                        } else map.put(secondWord, count + 1);// если есть в списке, то +1
                    }
                    System.out.println("");
                   // System.out.println("Слова в списке книг и количество их повторений: ");
                    System.out.println(map);
                    //System.out.println(map.values());
                }

                //считываение одним заходом в строку. Используется то, что в любых кодировках предельное значение числа символом всегда меньше или равно числу байтов.
                //File f = new File("src\\main\\resources\\booksTanya.txt");
                File f = new File(SRC_MAIN_RESOURCES + iName);

                final int length = (int) f.length();
                if (length != 0) {
                    char[] cbuf = new char[length];
                    InputStreamReader isr = new InputStreamReader(new FileInputStream(f),"windows-1251");
                    final int read = isr.read(cbuf);
                    String s = new String(cbuf, 0, read);
                    System.out.println(s);
                    isr.close();
                }
            }
            else if (listOfFiles[i].isDirectory()) {
                for (int j = 0; j < depth; j++) System.out.print("\t");
                System.out.println("Directory: "+iName);
                findFiles(listOfFiles[i], depth+1);
            }
        }
    }
    public static void splitToWords(String s) throws IOException {
        List<String> lines = FileUtils.readLines(new File("src\\main\\resources\\books.txt"));
        for (String line : lines) {
            String[] words = line.split("[\\.\\s-]+");
            for (int i = 0; i < words.length; i++) {
                System.out.print(words[i] + " | ");
            }
            System.out.println("");
        }
    }
}
