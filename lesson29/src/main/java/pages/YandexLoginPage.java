package pages;

import org.openqa.selenium.By;

/**
 * Created by User on 05.04.2017.
 */
public class YandexLoginPage extends AbstractPage {


    private static final String BASE_URL = "https://mail.yandex.ru";
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='passwd']");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//span[contains(@class,'new-auth-submit')]/*[@type=\"submit\"]");

    public YandexLoginPage open() {
        driver.get(BASE_URL);
        return this;
    }


    public YandexLoginPage inputLogin(String login) {
        driver.findElement(LOGIN_INPUT_LOCATOR).sendKeys(login);
        return this;
    }

    public YandexLoginPage inputPassword(String password) {
        driver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(password);
        return this;
    }

    public YandexInboxPage clickLoginButton() {
        driver.findElement(LOGIN_BUTTON_LOCATOR).click();
        return new YandexInboxPage();
    }
}
