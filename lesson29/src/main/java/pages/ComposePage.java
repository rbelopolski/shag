package pages;

import org.openqa.selenium.By;

/**
 * Created by Roma on 09.04.2017.
 */
public class ComposePage extends AbstractPage  {
    private static final By NAME_INPUT_LOCATOR = By.xpath(".//*[@name=\"to\"]");
    private static final By CLICK_SEND_LOCATOR = By.xpath(".//*[@id='nb-34']");
    private static final By INBOX_BUTTON_LOCATOR = By.xpath(".//*[@id='nb-1']/body/div[2]/div[4]/div/div[2]/div[2]/div[2]/div[2]/div[1]/a[1]/span");
    private static final String BASE_URL = "https://mail.yandex.ru";
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='passwd']");
    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//span[contains(@class,'new-auth-submit')]/*[@type=\"submit\"]");

    public ComposePage open() {
        driver.get(BASE_URL);
        return this;
    }


    public ComposePage inputLogin(String login) {
        driver.findElement(LOGIN_INPUT_LOCATOR).sendKeys(login);
        return this;
    }

    public ComposePage inputPassword(String password) {
        driver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(password);
        return this;
    }
    public ComposePage inputNameTo(String nameto) {
        driver.findElement(NAME_INPUT_LOCATOR).sendKeys(nameto);
        return this;
    }
    public ComposePage clickSendMail() {
        driver.findElement(CLICK_SEND_LOCATOR).click();
        return this;
    }

    public YandexInboxPage clickInboxButton() {
        driver.findElement(INBOX_BUTTON_LOCATOR).click();
        return new YandexInboxPage();
    }
    public ComposePage clickLoginButton() {
        driver.findElement(LOGIN_BUTTON_LOCATOR).click();
        return this;
    }



}