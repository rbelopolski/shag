package pages;

import org.openqa.selenium.By;

/**
 * Created by User on 05.04.2017.
 */
public class YandexInboxPage extends AbstractPage{

    private static final By LOGIN_NAME_LOCATOR = By.xpath("//div[@class='mail-User-Name']");
    private static final By MAIL_NAME_LOCATOR = By.xpath(".//*[@title=\"testone.testone@yandex.ru\"]");
    public boolean isLoginNameDisplayed() {
        return driver.findElement(LOGIN_NAME_LOCATOR).isDisplayed();
    }

    public boolean isMailNameDisplayed() {return driver.findElement(MAIL_NAME_LOCATOR).isDisplayed();
    }
}
