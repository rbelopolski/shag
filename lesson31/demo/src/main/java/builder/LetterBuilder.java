package builder;

import bo.Letter;

import java.util.Random;

/**
 * Created by Main on 10.04.2017.
 */
public class LetterBuilder {
    public static Letter createLetter(){
        String recepient = "sashatest2017@yandex.by";
        String subject = "test letter " + new Random().nextInt(99999);
        String body = "You won " + new Random().nextInt(99999) + " dollars!!!";
        return new Letter(recepient, subject, body);
    }
}
