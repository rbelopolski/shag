package pages;

import org.openqa.selenium.By;

/**
 * Created by Account on 05.04.2017.
 */
public class InboxPage extends BasePage{

    private static final By LOGIN_NAME_LOCATOR = By.xpath("//div[@class='mail-Account-Name']");
    private static final By NEW_LETTER_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");

    public boolean isLoginNameDisplayed() {
        return driver.findElement(LOGIN_NAME_LOCATOR).isDisplayed();
    }

    public ComposePage clickNewLetter() {
        driver.findElement(NEW_LETTER_BUTTON_LOCATOR).click();
        return new ComposePage();
    }
}
