package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.*;


public class YandexMailTests {

    @Test(description = "Yandex login test")
    public void YandexMailLoginTest() {
        YandexMailInboxPage mailInboxPage = new YandexMailLoginPage().open().inputLogin("sashatest2017").inputPassword("sashatest20").clickLoginButton();
        Assert.assertTrue(mailInboxPage.isLoginNameDisplayed(), "Incorrect login or password!");
        //YandexMailInboxPage.close();
    }

    @Test(description = "Yandex new letter test")
    public void YandexMailComposeTest() {
        YandexMailLoginTest();
        YandexMailComposePage mailComposePage = new YandexMailInboxPage().clickNewLetterButton();
        Assert.assertTrue(mailComposePage.isFieldCaptionDisplayed(), "Don't compose new letter!");
        //YandexMailComposePage.close();
    }

    @Test(description = "Yandex done letter test")
    public void YandexMailDoneTest() {
        YandexMailComposeTest();
        YandexMailDonePage mailDonePage = new YandexMailComposePage().inputAddressee("sashatest2017@yandex.by").inputTextLetter("New letter.").clickSendButton();
        Assert.assertTrue(mailDonePage.isSendLetterDisplayed(), "Don't send letter!");
        //YandexMailDonePage.close();
    }

    @Test(description = "Yandex new inbox letter test")
    public void YandexMailInboxTest() {
        YandexMailDoneTest();
        YandexMailUnreadPage mailUnreadPage = new YandexMailDonePage().clickUnreadButton();
        Assert.assertTrue(mailUnreadPage.isUnreadLetterDisplayed(), "Unread letter is not");
        YandexMailLetterPage mailLetterPage = new YandexMailUnreadPage().clickUnreadLetter();
        //YandexMailLetterPage.close();
    }
}
