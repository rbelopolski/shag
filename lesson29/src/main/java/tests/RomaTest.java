package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ComposePage;
import pages.YandexInboxPage;

/**
 * Created by Roma on 09.04.2017.
 */
public class RomaTest {
    @Test(description = "Mail yandex test")
    public void mailTest(){
        YandexInboxPage inboxPage = new ComposePage().open().inputLogin("testone.testone").inputPassword("yandex.ru").clickLoginButton().inputNameTo("testone.testone.yandex.ru").clickSendMail().clickInboxButton();
        Assert.assertTrue(inboxPage.isMailNameDisplayed());

    }
}
