import java.io.IOException;

/**
 * Created by Vel2 on 22.03.2017.
 */
public class Table {
    public static void main(String[] args) throws IOException {

        System.out.println("Добавление столбца на указанную позицию:");
        System.out.println();
        Methods.addColumn(8,"!!name!!", "!!val1!!", "!!val2!!", "!!val3!!", "!!val4!!");

        System.out.println();
        System.out.println();

        System.out.println("Удаление столбца с указанным именем:");
        System.out.println();
        Methods.removeColumn("year");

        System.out.println();
        System.out.println();

        System.out.println("Замена значений в столбце с указанным именем:");
        System.out.println();
        Methods.editColumn("model", "!!val1!!", "!!val2!!");

    }
}

