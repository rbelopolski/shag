| brand | engine_volume | body      | color | power | transmission | fuel   |
-----------------------------------------------------------------------------
| audi  |  4.5          | sport     | red   | 250   | auto         | petrol |
| BMW   | 3.5           | crossover | black | 200   | auto         | diesel |
| opel  |   2.5         | sedan     | white | 140   | mechanic     | diesel |