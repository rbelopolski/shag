import java.util.*;

/**
 * Created by User on 20.03.2017.
 */
public class Record {
    public static void printSortMap(Map<String, Integer> mapSorting) {
        List list = new ArrayList(mapSorting.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> a, Map.Entry<String, Integer> b) {
                return b.getValue() - a.getValue();
            }
        });

        String[] keyAndValue;
        for (int i = 0; i < list.size(); i++) {
            keyAndValue = list.get(i).toString().split("[\\r\\n\\=\\s-]+");
            if (keyAndValue[1].length() == 2) {
                System.out.println(keyAndValue[1] + " :  " + keyAndValue[0]);
            } else {
                System.out.println(keyAndValue[1] + "  :  " + keyAndValue[0]);
            }
        }
    }
}
